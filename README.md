# A project with React & Material UI
Made following a youtube tuto from [CodeRealm](https://www.youtube.com/channel/UCUDLFXXKG6zSA1d746rbzLQ)  
Here the [playlist](https://www.youtube.com/playlist?list=PLcCp4mjO-z98WAu4sd0eVha1g-NMfzHZk) !


## License

The repo is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## List of the video | Progression

 - [x] 1\. Intro
 - [x] 2\. Grid Layout
 - [x] 3\. Data Store
 - [x] 4\. Lists, Tabs & Typography
 - [x] 5\. Dialogs & Icons
 - [x] 6\. Forms, Inputs & Styling
 - [x] 7\. Lists & IconButtons
 - [x] 8\. Forms (Part 1)
 - [x] 9\. Forms (Part 2)
 - [x] 10\. CSS-in-JS
 - [x] 11\. Styling with JSS
 - [x] 12\. Theming (Part 1)
 - [x] 13\. Theming (Part 2)
 - [x] 14\. Theming (Part 3)
 - [x] 15\. Context API (React 16.3+)
 - [x] 16.01\. Deconstructing CRA with Webpack 4 & Babel 7 
 - [x] 16\. Optimizing Bundle Size
 - [x] 17\. Server-Side Rendering
 - [x] 18\. Refactoring
 - [x] 19\. Outro (Finale)


## Tips 

__6\. Forms, Inputs & Styling:__  
I recommended to set a DEFAULT_STATE in ExerciseDialogseDialog.js 

__7\. Lists & IconButtons:__  
There is 3 way to import rounded Icons.  
I prefer import icons this way _import { CreateRounded, DeleteRounded } from "@material-ui/icons";_  
 See more  src/Components/Exercises/client.js
 
__16.01. Deconstructing CRA with Webpack 4 & Babel 7 :__  
Here the [link](https://www.youtube.com/watch?v=A4swyDR45SY) of the video.  
You have to do that before __16__ (it's specify on it description) 