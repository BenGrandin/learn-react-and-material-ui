const nodeExternals = require("webpack-node-externals");

const common = {
	devtool: "cheap-module-source-map",
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
				},
			},
		],
	},
	resolve: {
		alias: {
			"@material-ui/core": "@material-ui/core/es",
			"@material-ui/icons": "@material-ui/icons/esm",
		},
	},
};

module.exports = [
	{
		...common,
		entry: "./src/client",
		output: {
			path: `${__dirname}/public`,
		},
	},
	{
		...common,
		target: "node",
		entry: "./src/server",
		externals: [nodeExternals()],
	},
];