import { ServerStyleSheets, ThemeProvider } from "@material-ui/styles";
import express from "express";
// import { SheetsRegistry } from "jss";
import React from "react";
import ReactDOMServer from "react-dom/server";
import reload from "reload";
// import { JssProvider } from "react-jss";
import App from "./Components/App";
import theme from "./theme";


const app = express();
const port = 3000;
const dev = process.env.NODE_ENV === "development";
app.use(express.static("public"));

if ( dev ) reload(app);

app.use((req, res) => {
	const sheets = new ServerStyleSheets();

	const html = ReactDOMServer.renderToString(
		sheets.collect(
			<ThemeProvider theme={theme}>
				<App/>
			</ThemeProvider>,
		),
	);
	const css = sheets.toString();

	res.send(`<!DOCTYPE html>
		<html lang="en">
		
		<head>
		    <meta charset="utf-8">
		    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
		    <title>React App</title>
		    <style id="jss-style">${css}</style>
		</head>
		
		<body>
		<div id="root">${html}</div>
		<script src="main.js" ></script>
      ${dev ? `<script src='/reload/reload.js' async></script>` : ""}
		</body>
		
		</html>
	`);
});

app.listen(port, () => console.log(`http://localhost:${port}`));
