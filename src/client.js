import { ThemeProvider } from "@material-ui/core/styles";
import { createGenerateClassName } from "@material-ui/styles";
import React from "react";
import { hydrate } from "react-dom";
import { JssProvider } from "react-jss";
import App from "./Components/App";
import theme from "./theme";


const generateClassName = createGenerateClassName();

hydrate(
	<JssProvider generateClassName={generateClassName}>
		<ThemeProvider theme={theme}>
			<App/>
		</ThemeProvider>
	</JssProvider>,
	document.getElementById("root"),
	() => {
		const temp = document.getElementById("jss-styles");
		if ( temp ) temp.remove();
	},
);