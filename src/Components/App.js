import CssBaseline from "@material-ui/core/CssBaseline";
import React, { PureComponent } from "react";
import { Provider } from "../context";
import { exercises, muscles } from "../store";
import Exercises from "./Exercises";
import { Footer, Header } from "./Layouts";


class App extends PureComponent {
	state = {
		exercises,
		exercise: {},
		editMode: false,
		category: "",
	};

	getExercisesByMuscles() {
		const initExercises = muscles.reduce((acc, muscle) => ({
			...acc,
			[muscle]: [],
		}), {});

		return Object.entries(
			this.state.exercises.reduce((acc, current) => {
				const { muscles } = current;
				acc[muscles] = [...acc[muscles], current];

				return acc;
			}, initExercises));
	}

	onCategorySelect = (category) => {
		this.setState({ category });
	};

	onExerciseSelect = id => {
		this.setState(({ exercises }) => ({
			exercise: exercises.find(ex => ex.id === id),
			editMode: false,
		}));
	};

	onExerciseCreate = exercise => {
		this.setState(({ exercises }) => ({
			exercises: [...exercises, exercise],

		}));
	};

	onExerciseDelete = id => {
		this.setState(({ exercises, exercise }) => {
			const newState = {
				exercises: exercises.filter(ex => ex.id !== id),
			};

			if ( exercise.id === id ) {
				newState.exercise = {};
				newState.editMode = true;
			}

			return newState;
		});
	};

	onSelectEdit = id => {
		this.setState(({ exercises }) => ({
			exercise: exercises.find(ex => ex.id === id),
			editMode: true,
		}));
	};

	onExerciseEdit = exercise => {
		this.setState(({ exercises }) => ({
			exercises: [
				...exercises.filter(ex => ex.id !== exercise.id),
				exercise],
			exercise,
		}));
	};

	getContext = () => ({
		muscles,
		...this.state,
		onCategorySelect: this.onCategorySelect,
		onExerciseCreate: this.onExerciseCreate,
		onExerciseSelect: this.onExerciseSelect,
		onSelectEdit: this.onSelectEdit,
		onExerciseEdit: this.onExerciseEdit,
		onExerciseDelete: this.onExerciseDelete,
		exercisesByMuscles: this.getExercisesByMuscles(),
	});

	render() {

		return <Provider value={this.getContext()}>
			<CssBaseline/>

			<Header/>
			<Exercises/>
			<Footer/>

		</Provider>;
	}
}

export default App;
