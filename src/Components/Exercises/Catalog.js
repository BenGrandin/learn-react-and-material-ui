import {
	IconButton,
	List,
	ListItem,
	ListItemSecondaryAction,
	ListItemText,
	Typography,
	withStyles,
} from "@material-ui/core";

import { CreateRounded, DeleteRounded } from "@material-ui/icons";
import React, { Fragment } from "react";
import { withContext } from "../../context";


const styles = {
	Capitalize: {
		textTransform: "capitalize",
	},
};

const Catalog = ({
	category,
	classes,
	onExerciseSelect,
	onSelectEdit,
	onExerciseDelete,
	exercisesByMuscles,

}) => {

	return (
		exercisesByMuscles.map(([key, values]) =>
			( !category || category === key) &&
			<Fragment key={key}>
				<Typography color={"secondary"}
				            variant={"h5"}
				            className={classes.Capitalize}
				>
					{key}
				</Typography>

				<List component="ul">
					{values.map(({ id, title }) => {
						return <ListItem key={id}
						                 button
						                 onClick={() => onExerciseSelect(
							                 id)}>

							<ListItemText primary={title}/>

							<ListItemSecondaryAction>
								<IconButton color={"primary"}
								            onClick={() => onSelectEdit(
									            id)}>
									<CreateRounded/>
								</IconButton>
								<IconButton color={"primary"}
								            onClick={() => onExerciseDelete(
									            id)}>
									<DeleteRounded/>
								</IconButton>
							</ListItemSecondaryAction>


						</ListItem>;
					})}

				</List>

			</Fragment>
			,
		));
};

export default withContext(withStyles(styles)(Catalog));